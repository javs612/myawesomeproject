package pageMethods;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PageActions {
	
	public static void dropDownSelectValue(WebElement webel, String valor) {
		Select dlSelect = new Select(webel);
		dlSelect.selectByValue(valor);
		System.out.println("--*-- "+ dlSelect.getFirstSelectedOption().getText());

	}
	
	public static void dropDownSelectByText(WebElement webel, String text) {
		Select dlSelect = new Select(webel);
		dlSelect.selectByVisibleText(text);	
		System.out.println("--*-- "+ dlSelect.getFirstSelectedOption().getText());
	}
	
	public static void fillInput(WebElement webel, String text) {
		webel.clear();
		webel.sendKeys(text);
		System.out.println("--*-- "+ webel.getAttribute("value"));
	}
	
	public static void clickElement(WebElement webel) {
		webel.click();
	}
	
	public static void clickElementIfDisplayed(WebElement webel) {
		if (verifyElementDisplayed(webel)) {
			webel.click();
		}
		
	}
	
	public static boolean verifyElementDisplayed(WebElement webel) {
		return webel.isDisplayed();
	}
}

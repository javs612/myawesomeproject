package config;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
public class Setup {

	private static WebDriver driver;
	static String chromePath = System.getProperty("user.dir")+"/drivers/chromedriver.exe";
	static String fireFoxPath = System.getProperty("user.dir")+"/drivers/geckodriver.exe";
	static String iePath = System.getProperty("user.dir")+"/drivers/IEDriverServer.exe";
	
	public Setup(String browser) {
		switch (browser) {
		case "chrome":
			System.setProperty("webdriver.chrome.driver", chromePath);
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			//Navigate espera a que se termine de cargar la página
			driver.navigate().to("http://newtours.demoaut.com/");
			driver.manage().window().maximize();
			break;
		case "ie":
			System.setProperty("webdriver.ie.driver", iePath);
			driver = new InternetExplorerDriver();
			
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			//Navigate espera a que se termine de cargar la página
			driver.navigate().to("http://newtours.demoaut.com/");
			driver.manage().window().maximize();
			break;
		case "firefox":
			System.setProperty("webdriver.gecko.driver", fireFoxPath);
			driver = new FirefoxDriver();
			
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			//Navigate espera a que se termine de cargar la página
			driver.navigate().to("http://newtours.demoaut.com/");
			driver.manage().window().maximize();
			break;
			default:
				System.setProperty("webdriver.chrome.driver", chromePath);
				driver = new ChromeDriver();
				
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				//Navigate espera a que se termine de cargar la página
				driver.navigate().to("http://newtours.demoaut.com/");
				driver.manage().window().maximize();
				break;
		}
		
	}
	
	public WebDriver getDriver() {
		return driver;
	}
	
	public void close () {
		driver.quit();
	}

}

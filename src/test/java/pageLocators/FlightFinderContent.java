package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FlightFinderContent {
		
	@FindBy (xpath = "/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td/font")
	public WebElement flightFinderText;
	
	@FindBy (xpath = "//input[@name='findFlights' and @type='image']")
	public WebElement continueBtn;
	
	// Flight Details Section , departing and arriving and dates and amount of passengers
	
	@FindBy (xpath = "//input[@name='tripType' and @value='roundtrip']")
	public WebElement roundTripRadioBtn;
	
	@FindBy (xpath = "//input[@name='tripType' and @value='oneway']")
	public WebElement oneWayRadioBtn;
	
	@FindBy (xpath = "//select[@name='passCount']")
	public WebElement passengerSelect;
	
	@FindBy (xpath = "//select[@name='fromPort']")
	public WebElement departingSelect;
	
	@FindBy (xpath = "//select[@name='fromMonth']")
	public WebElement departingMonthSelect;
	
	@FindBy (xpath = "//select[@name='fromDay']")
	public WebElement departingDaySelect;
	
	@FindBy (xpath = "//select[@name='toPort']")
	public WebElement arrivingSelect;
	
	@FindBy (xpath = "//select[@name='toMonth']")
	public WebElement returningMonthSelect;
	
	@FindBy (xpath = "//select[@name='toDay']")
	public WebElement returningDaySelect;
	
	//Preferences 
	
	@FindBy (xpath = "//input[@name='servClass' and @value='Coach']")
	public WebElement economyClassRadioBtn;
	
	@FindBy (xpath = "//input[@name='servClass' and @value='Business']")
	public WebElement businessClassRadioBtn;
	
	@FindBy (xpath = "//input[@name='servClass' and @value='First']")
	public WebElement firstClassRadioBtn;
	
	@FindBy (xpath = "//select[@name='airline']")
	public WebElement airlinePrefselect;
	
}

package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignOnContent {
	
	@FindBy (xpath = "//input[@name='userName']") 
	public WebElement userNameInput;
	
	
	@FindBy (xpath = "//input[@name='password']") 
	public WebElement passwordInput;
	
	@FindBy (xpath = "//input[@name='login']") 
	public WebElement submitBtn;
}

package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SummaryPageContent {
	
	@FindBy (xpath="//form[@name='bookflight']")
	public WebElement FormSummary;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]/b/font")
	public WebElement departToTxt;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td/table/tbody/tr[3]/td[1]/font/b")
	public WebElement departflight;
		
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/font")
	public WebElement flightClassTxt;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td/table/tbody/tr[3]/td[3]/font")
	public WebElement departflightPrice;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td/table/tbody/tr[4]/td[1]/b/font")
	public WebElement returnToTxt;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td/table/tbody/tr[6]/td[3]/font")
	public WebElement returnflightPrice;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[2]/td/table/tbody/tr[6]/td[1]/font/font/font[1]/b")
	public WebElement returnflight;
	
	@FindBy (xpath="//select[@name='creditCard']")
	public WebElement creditCardType;
	
	@FindBy (xpath="//input[@name='creditnumber']")
	public WebElement creditcardNumber;
	
	@FindBy (xpath="//select[@name='cc_exp_dt_mn']")
	public WebElement expirationMonth;
	
	@FindBy (xpath="//select[@name='cc_exp_dt_yr']")
	public WebElement expirationYear;
	
	@FindBy (xpath="//input[@name='cc_frst_name']")
	public WebElement ccFirstName;
	
	@FindBy (xpath="//input[@name='cc_mid_name']")
	public WebElement ccMidName;
	
	@FindBy (xpath="//input[@name='cc_last_name']")
	public WebElement ccLastName;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[9]/td[2]/input")
	public WebElement ticketLessCheckbx;
	
	@FindBy (xpath="//input[@name='billAddress1']")
	public WebElement billAddress1;
	
	@FindBy (xpath="//input[@name='billAddress2']")
	public WebElement billAddress2;
	
	@FindBy (xpath="//input[@name='billCity']")
	public WebElement billCity;
	
	@FindBy (xpath="//input[@name='billState']")
	public WebElement billState;
	
	@FindBy (xpath="//input[@name='billZip']")
	public WebElement billZipcode;
	
	@FindBy (xpath="//select[@name='billCountry']")
	public WebElement billCountry;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[15]/td[2]/input")
	public WebElement sameBillAdd;
	
	@FindBy (xpath="//input[@name='delAddress1']")
	public WebElement delAddress1;
	
	@FindBy (xpath="//input[@name='delAddress2']")
	public WebElement delAddress2;
	
	@FindBy (xpath="//input[@name='delCity']")
	public WebElement delCity;
	
	@FindBy (xpath="//input[@name='delState']")
	public WebElement delState;
	
	@FindBy (xpath="//input[@name='delZip']")
	public WebElement delZipcode;
	
	@FindBy (xpath="//select[@name='delCountry']")
	public WebElement delCountry;
	
	@FindBy (xpath="//input[@name='buyFlights']")
	public WebElement btnPurchase;
	

}

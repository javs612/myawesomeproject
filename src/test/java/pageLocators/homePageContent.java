package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class homePageContent {
	
	@FindBy (linkText = "SIGN-OFF") 
	public WebElement logOut;
	
	
	@FindBy (linkText = "SIGN-ON") 
	public WebElement logIn;
}

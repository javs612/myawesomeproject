package pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FlightSelectionContent {
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table[1]/tbody")
	public WebElement tabledepart;
	
	@FindBy (xpath="/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table[2]/tbody")
	public WebElement tableReturn;
	
	@FindBy (xpath="//input[@type='hidden' and @name='fromPort']")
	public WebElement DepartingPort;
	
	@FindBy (xpath="//input[@type='hidden' and @name='toPort']")
	public WebElement ArrivalPort;
	
	@FindBy (xpath="//input[@type='hidden' and @name='fromMonth']")
	public WebElement DepartMonth;
	
	@FindBy (xpath="//input[@type='hidden' and @name='fromDay']")
	public WebElement DepartDay;
	
	@FindBy (xpath="//input[@type='hidden' and @name='toMonth']")
	public WebElement ReturnMonth;
	
	@FindBy (xpath="//input[@type='hidden' and @name='toDay']")
	public WebElement ReturnDay;
	
	@FindBy (xpath="//input[@type='image' and @name='reserveFlights']")
	public WebElement ContinueBtn;
	
}

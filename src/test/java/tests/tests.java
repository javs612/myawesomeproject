package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import config.Setup;
import pageMethods.PageActions;
import pageObjects.ConfirmationPage;
import pageObjects.FlightFinderPage;
import pageObjects.FlightSelectionPage;
import pageObjects.HomePage;
import pageObjects.SignOnPage;
import pageObjects.SummaryPage;
import utils.ExcelUtils;

public class tests {
	public static WebDriver driver;
	static Object[][] testObjArray;
	String testCaseWorkBook = System.getProperty("user.dir") + "/resources/WorkBook.xls";
	public static Setup setup;
	public  SignOnPage signonPage;
	public  HomePage homepage;
	public  FlightFinderPage ffPage;
	public  FlightSelectionPage fspage;
	public  SummaryPage sumPage;
	public 	ConfirmationPage confpage; 

	
	@DataProvider(name = "UserRegistration")
	public Object[][] userRegister() throws Exception{
		testObjArray = ExcelUtils.getTableArray(testCaseWorkBook, "Sheet1");
		return (testObjArray);
	}

	@Test(dataProvider = "UserRegistration", description="Test Case for logging in of a user")
	public void TestSignOn(String...registerInfo) throws InterruptedException{
		setup = new Setup(registerInfo[0]);
		driver = setup.getDriver();
		signonPage = new SignOnPage(driver);
		homepage = new HomePage(driver);
		ffPage = new FlightFinderPage(driver);
		fspage = new FlightSelectionPage(driver);
		sumPage = new SummaryPage(driver);
		confpage = new ConfirmationPage(driver);
		PageActions.clickElement(homepage.SignIn());
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		PageActions.fillInput(signonPage.UserName(),registerInfo[1]);
		PageActions.fillInput(signonPage.Password(), registerInfo[2]);
		PageActions.clickElement(signonPage.Submit());
		
		
		driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
		
		Assert.assertEquals(PageActions.verifyElementDisplayed(ffPage.Passenger()), true);
		PageActions.clickElement(ffPage.TripType(registerInfo[3]));
		PageActions.dropDownSelectByText(ffPage.Passenger(),registerInfo[4]);
		PageActions.dropDownSelectByText(ffPage.Departing(),registerInfo[5]);
		PageActions.dropDownSelectValue(ffPage.DepartMonth(),registerInfo[6]);
		PageActions.dropDownSelectByText(ffPage.DepartDay(),registerInfo[7]);
		PageActions.dropDownSelectByText(ffPage.Arriving(),registerInfo[8]);
		PageActions.dropDownSelectValue(ffPage.ReturnMonth(),registerInfo[9]);
		PageActions.dropDownSelectByText(ffPage.ReturnDay(),registerInfo[10]);
		PageActions.clickElement(ffPage.TypeClass(registerInfo[11]));
		PageActions.dropDownSelectByText(ffPage.AirlinePref(),registerInfo[12]);
		PageActions.clickElement(ffPage.ContinueBtn());
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		Assert.assertEquals(PageActions.verifyElementDisplayed(fspage.TableDepart()), true);
		Assert.assertEquals(fspage.DepartingPort().getAttribute("value"), registerInfo[5]);

		Assert.assertEquals(fspage.ArrivalPort().getAttribute("value"), registerInfo[8]);
		
		//Fecha es igual verify
		Assert.assertEquals(fspage.DepartMonth().getAttribute("value"), registerInfo[6]);
		Assert.assertEquals(fspage.DepartDay().getAttribute("value"), registerInfo[7]);
		
		Assert.assertEquals(fspage.ReturnMonth().getAttribute("value"), registerInfo[9]);
		Assert.assertEquals(fspage.ReturnDay().getAttribute("value"), registerInfo[10]);

		System.out.println("--*-- FLIGHT OPTIONS ");

		PageActions.clickElement(fspage.DepartFlight(0, "asc"));
		PageActions.clickElement(fspage.ReturnFlight(2, "desc"));
		PageActions.clickElement(fspage.Continue());

		
		Assert.assertEquals(sumPage.DepartFlight(fspage.DepartingFlight).booleanValue(), true);
		Assert.assertEquals(sumPage.returnFlight(fspage.returnFlight).booleanValue(), true);
		Assert.assertEquals(sumPage.DepartingToTxt(registerInfo[5], registerInfo[8]).booleanValue(), true);
		if(registerInfo[11] == "economy") {
			Assert.assertEquals(sumPage.flightClass("Coach").booleanValue(), true);
		}else {
			Assert.assertEquals(sumPage.flightClass(registerInfo[11]).booleanValue(), true);
		}
		
		// passengers
		System.out.println("--*--  PASSENGERS INFORMATION AND MEAL");

		for(int i = 0 ; i < Integer.parseInt(registerInfo[4]) ; i++) {
			PageActions.fillInput(sumPage.passengerFirstName(i), registerInfo[13+(i*3)]);
			PageActions.fillInput(sumPage.passengerLastName(i), registerInfo[14+(i*3)]);
			PageActions.dropDownSelectByText(sumPage.passengerMeal(i), registerInfo[15+(i*3)]);
		}
		
		// billing and delivery 
		System.out.println("--*-- CREDIT CARD AND BILLING INFORMATION");

		PageActions.dropDownSelectByText(sumPage.cctype(),registerInfo[25]);
		PageActions.fillInput(sumPage.ccNumber(),registerInfo[26]);
		PageActions.dropDownSelectByText(sumPage.ccExpMonth(),registerInfo[27]);
		PageActions.dropDownSelectValue(sumPage.ccExpYr(),registerInfo[28]);
		PageActions.fillInput(sumPage.ccFirstName(),registerInfo[29]);

		PageActions.fillInput(sumPage.ccMidName(),registerInfo[30]);
		PageActions.fillInput(sumPage.ccLastName(),registerInfo[31]);
		
		if(registerInfo[32] == "yes") {
			PageActions.clickElement(sumPage.ticketless());
		}
		
		PageActions.fillInput(sumPage.billAddress1(),registerInfo[33]);
		PageActions.fillInput(sumPage.billAddress2(),registerInfo[34]);
		PageActions.fillInput(sumPage.billCity(),registerInfo[35]);
		PageActions.fillInput(sumPage.billState(),registerInfo[36]);
		PageActions.fillInput(sumPage.billZipcode(),registerInfo[37]);
		PageActions.dropDownSelectByText(sumPage.billCountry(),registerInfo[38]);
		
		System.out.println("--*-- DELIVERY SECTION");

		if(registerInfo[39] == "yes") {
			PageActions.clickElement(sumPage.sameBillAdd());
		}
		
		PageActions.fillInput(sumPage.delAddress1(),registerInfo[40]);
		PageActions.fillInput(sumPage.delAddress2(),registerInfo[41]);
		PageActions.fillInput(sumPage.delCity(),registerInfo[42]);
		PageActions.fillInput(sumPage.delState(),registerInfo[43]);
		PageActions.fillInput(sumPage.delZipcode(),registerInfo[44]);
		PageActions.dropDownSelectByText(sumPage.delCountry(),registerInfo[45]);
		
		PageActions.clickElement(sumPage.continueBtn());
		
		// confirmation page
		Assert.assertEquals(confpage.confirmationTxt("Your itinerary has been booked!"),true);
		Assert.assertEquals(confpage.DepartingToTxt(registerInfo[5], registerInfo[8]), true);
		Assert.assertEquals(confpage.DepartFlight(fspage.DepartingFlight), true);
		if(registerInfo[11] == "economy") {
			Assert.assertEquals(confpage.DepartFlightClass("Coach"), true);
		}else {
			Assert.assertEquals(confpage.DepartFlightClass(registerInfo[11]), true);
		}


		
		Assert.assertEquals(confpage.ReturnToTxt(registerInfo[8], registerInfo[5]), true);
		Assert.assertEquals(confpage.ReturnFlight(fspage.returnFlight), true);
		if(registerInfo[11] == "economy") {
			Assert.assertEquals(confpage.ReturntFlightClass("Coach"), true);
		}else {
			Assert.assertEquals(confpage.ReturntFlightClass(registerInfo[11]), true);
		}

		Assert.assertEquals(confpage.Passengers(registerInfo[4]), true);
		Assert.assertEquals(confpage.billingName(registerInfo[29],registerInfo[30],registerInfo[31]), true);
		Assert.assertEquals(confpage.billAdress1(registerInfo[33]), true);
		Assert.assertEquals(confpage.billAddress2(registerInfo[34]), true);
		Assert.assertEquals(confpage.billcitstatezip(registerInfo[35], registerInfo[36], registerInfo[37]), true);
		
		Assert.assertEquals(confpage.delAddress1(registerInfo[40]), true);
		Assert.assertEquals(confpage.delAddress2(registerInfo[41]), true);
		Assert.assertEquals(confpage.delcitstatezip(registerInfo[42], registerInfo[43], registerInfo[44]), true);
		
		confpage.taxes();
		confpage.totalPrice();
		
		PageActions.clickElement(confpage.logout());
		
		setup.close();
	}

}

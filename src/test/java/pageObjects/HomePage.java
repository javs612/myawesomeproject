package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import pageLocators.homePageContent;

public class HomePage {
	
	private WebDriver driver;
	public homePageContent homepagecont;
	
	public HomePage(WebDriver webDriver) {
		driver = webDriver;
		homepagecont = PageFactory.initElements(driver, homePageContent.class);
	}
	
	public WebElement SignIn() {
		return homepagecont.logIn;
	}
	
	public WebElement SignOff() {
		return homepagecont.logOut;
	}
}

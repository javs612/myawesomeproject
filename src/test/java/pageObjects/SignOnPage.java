package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import pageLocators.SignOnContent;

public class SignOnPage {
	// in test call my class setup where i have a function to open browser where i send a 
	// parameter which is the parameter to open , the constructor for the class setup
	// defaulty constrctor will open chrome
	// other construstor is parameters from data provider which is sent at the start of my test
	// when my test is done call @aftermethod or @after test to close browser
	// each new row of data to test will call the setup to open the browser selcted for that set of data
	
	private WebDriver driver;
	public SignOnContent signOnContent;
	
	public SignOnPage(WebDriver webDriver) {
		driver = webDriver;
		signOnContent = PageFactory.initElements(driver, SignOnContent.class);
	}
	
	public WebElement UserName() {
		return signOnContent.userNameInput;
	}
	
	public WebElement Password() {
		return signOnContent.passwordInput;
	}
	
	public WebElement Submit() {
		return signOnContent.submitBtn;
	}

}

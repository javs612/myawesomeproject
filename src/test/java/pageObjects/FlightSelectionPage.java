package pageObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import pageLocators.FlightSelectionContent;

public class FlightSelectionPage {
	private WebDriver driver;
	public FlightSelectionContent flightselectcont;
	public String returnFlight = "";
	public String DepartingFlight = "";
	public String DepartingPrice = "";
	public String ReturningPrice = "";
	
	public FlightSelectionPage(WebDriver webDriver) {
		driver = webDriver;
		flightselectcont = PageFactory.initElements(driver, FlightSelectionContent.class);
	}
	
	public WebElement ArrivalPort() {
		return flightselectcont.ArrivalPort;
	}
	
	public WebElement DepartingPort() {
		return flightselectcont.DepartingPort;
	}
	public WebElement TableDepart() {
		return flightselectcont.tabledepart;
	}
	public WebElement DepartMonth() {
		return flightselectcont.DepartMonth;
	}
	public WebElement DepartDay() {
		return flightselectcont.DepartDay;
	}
	
	public WebElement Continue() {
		return flightselectcont.ContinueBtn;
	}
	
	public WebElement ReturnMonth() {
		return flightselectcont.ReturnMonth;
	}
	public WebElement ReturnDay() {
		return flightselectcont.ReturnDay;
	}

	public WebElement DepartFlight(int num, String order) {
		List<Integer> Prices = new ArrayList<Integer>();
		List<WebElement> DepartOptions = flightselectcont.tabledepart.findElements(By.xpath("./tr"));
		System.out.println("Depart Options: ");

		for(WebElement webel : DepartOptions) {
			if(DepartOptions.indexOf(webel) > 1 && DepartOptions.indexOf(webel) %2 == 0) {
				System.out.println("Flight: " + webel.findElement(By.xpath(".//td[2]//font//b")).getText());
				System.out.println("Depart Time: " + webel.findElement(By.xpath(".//td[3]//font")).getText());
				System.out.println("Stops: " + webel.findElement(By.xpath(".//td[4]//font")).getText());
				System.out.println("Stops: " + webel.findElement(By.xpath(".//following-sibling::tr//td//font//font//b")).getText());

				Prices.add(Integer.parseInt(webel.findElement(By.xpath(".//following-sibling::tr//td//font//font//b")).getText().replaceAll("[^\\d.]", "")));
			}
		}
		if (order == "desc") {
			Collections.sort(Prices, Collections.reverseOrder());

		}else {
			Collections.sort(Prices);
		}
		DepartingPrice = Prices.get(num).toString();
		DepartingFlight = flightselectcont.tabledepart.findElement(By.xpath("./*[contains(., '"+Prices.get(num).toString()+"')]/preceding-sibling::tr[1]/td[2]/font/b")).getText();
		return flightselectcont.tabledepart.findElement(By.xpath("./*[contains(., '"+Prices.get(num).toString()+"')]/preceding-sibling::tr[1]/td[1]/input"));
	}
	
	public WebElement ReturnFlight(int num, String order) {
		List<Integer> Prices = new ArrayList<Integer>();
		List<WebElement> ArrivalOptions = flightselectcont.tableReturn.findElements(By.xpath("./tr"));
		System.out.println("Arrival Options: ");

		for(WebElement webel : ArrivalOptions) {
			if(ArrivalOptions.indexOf(webel) > 1 && (ArrivalOptions.indexOf(webel)  %2 == 0)) {
				System.out.println("Flight: " + webel.findElement(By.xpath(".//td[2]//font//b")).getText());
				System.out.println("Depart Time: " + webel.findElement(By.xpath(".//td[3]//font")).getText());
				System.out.println("Stops: " + webel.findElement(By.xpath(".//td[4]//font")).getText());
				System.out.println("Stops: " + webel.findElement(By.xpath(".//following-sibling::tr//td//font//font//b")).getText());

				Prices.add(Integer.parseInt(webel.findElement(By.xpath(".//following-sibling::tr//td//font//font//b")).getText().replaceAll("[^\\d.]", "")));
			}
		}
		if (order == "desc") {
			Collections.sort(Prices, Collections.reverseOrder());

		}else {
			Collections.sort(Prices);
		}
		ReturningPrice = Prices.get(num).toString();
		returnFlight = flightselectcont.tableReturn.findElement(By.xpath("./*[contains(., '"+Prices.get(num).toString()+"')]/preceding-sibling::tr[1]/td[2]/font/b")).getText();
		
		return flightselectcont.tableReturn.findElement(By.xpath("./*[contains(., '"+ Prices.get(num).toString() +"')]/preceding-sibling::tr[1]/td[1]/input"));
	}
}

package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import pageLocators.SummaryPageContent;

public class SummaryPage {
	private WebDriver driver;
	public SummaryPageContent summarypageCont;
	
	public SummaryPage(WebDriver webDriver) {
		driver = webDriver;
		summarypageCont = PageFactory.initElements(driver, SummaryPageContent.class);
	}
	
	public Boolean DepartingToTxt(String departing, String arriving) {
		
		if ( summarypageCont.departToTxt.getText().equalsIgnoreCase(departing+" to "+ arriving))
		{
			return true;
		}
		return false;
	}
	
	public Boolean DepartFlight(String departing) {
			
			if (summarypageCont.departflight.getText().equalsIgnoreCase(departing))
			{
				return true;
			}
			return false;
	}
	
	public Boolean flightClass(String classtype) {
		
		if (summarypageCont.flightClassTxt.getText().equalsIgnoreCase(classtype))
		{
			return true;
		}
		return false;
	}
	public Boolean returnFlight(String flight) {
		
		if (summarypageCont.returnflight.getText().equalsIgnoreCase(flight))
		{
			return true;
		}
		return false;
	}
	public Boolean returnFlightPrice(String flight) {
		
		if (summarypageCont.returnflightPrice.getText().equalsIgnoreCase(flight))
		{
			return true;
		}
		return false;
	}
	
	public Boolean returnFlightTo(String departing, String arriving) {
		
		if (summarypageCont.returnToTxt.getText().equalsIgnoreCase(departing+" to "+ arriving))
		{
			return true;
		}
		return false;
	}
	
	
	public WebElement passengerFirstName(int num) {	
		return summarypageCont.FormSummary.findElement(By.xpath(".//input[@name='passFirst"+num+"']"));
	}
	
	public WebElement passengerLastName(int num) {	
		return summarypageCont.FormSummary.findElement(By.xpath(".//input[@name='passLast"+num+"']"));
	}

	public WebElement passengerMeal(int num) {
		return summarypageCont.FormSummary.findElement(By.xpath(".//select[@name='pass."+num+".meal']"));
	}
	
	public WebElement cctype() {
		return summarypageCont.creditCardType;
	}
	
	public WebElement ccNumber() {
		return summarypageCont.creditcardNumber;
	}
	
	public WebElement ccExpMonth() {
		return summarypageCont.expirationMonth;
	}
	
	public WebElement ccExpYr() {
		return summarypageCont.expirationYear;
	}
	
	public WebElement ccFirstName() {
		return summarypageCont.ccFirstName;
	}
	
	public WebElement ccMidName() {
		return summarypageCont.ccMidName;
	}
	
	public WebElement ccLastName() {
		return summarypageCont.ccLastName;
	}
	
	public WebElement ticketless() {
		return summarypageCont.ticketLessCheckbx;
	}
	
	public WebElement billAddress1() {
		return summarypageCont.billAddress1;
	}
	
	public WebElement billAddress2() {
		return summarypageCont.billAddress2;
	}
	
	public WebElement billCity() {
		return summarypageCont.billCity;
	}
	
	public WebElement billState() {
		return summarypageCont.billState;
	}
	
	public WebElement billZipcode() {
		return summarypageCont.billZipcode;
	}
	
	public WebElement billCountry() {
		return summarypageCont.billCountry;
	}
	
	public WebElement sameBillAdd() {
		return summarypageCont.sameBillAdd;
	}
	
	public WebElement delAddress1() {
		return summarypageCont.delAddress1;
	}
	
	public WebElement delAddress2() {
		return summarypageCont.delAddress2;
	}
	
	public WebElement delCity() {
		return summarypageCont.delCity;
	}
	
	public WebElement delState() {
		return summarypageCont.delState;
	}
	
	public WebElement delCountry() {
		return summarypageCont.delCountry;
	}
	
	public WebElement delZipcode() {
		return summarypageCont.delZipcode;
	}
	
	public WebElement continueBtn() {
		return summarypageCont.btnPurchase;
	}
	
}

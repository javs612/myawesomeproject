package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import pageLocators.FlightFinderContent;

public class FlightFinderPage {

	private WebDriver driver;
	public FlightFinderContent flightfindercont;
	
	public FlightFinderPage(WebDriver webDriver) {
		driver = webDriver;
		flightfindercont = PageFactory.initElements(driver, FlightFinderContent.class);
	}
	
	public WebElement TripType(String type) {
		if(type == "one way") {
			return flightfindercont.oneWayRadioBtn;
		}else {
			return flightfindercont.roundTripRadioBtn;
		}
	}
	
	public WebElement Passenger() {
		return flightfindercont.passengerSelect;
	}
	
	public WebElement Departing() {
		return flightfindercont.departingSelect;
	}
	
	public WebElement DepartMonth() {
		return flightfindercont.departingMonthSelect;
	}
	
	public WebElement DepartDay() {
		return flightfindercont.departingDaySelect;
	}
	
	public WebElement Arriving() {
		return flightfindercont.arrivingSelect;
	}
	
	public WebElement ReturnMonth() {
		return flightfindercont.returningMonthSelect;
	}
	
	public WebElement ReturnDay() {
		return flightfindercont.returningDaySelect;
	}
	
	public WebElement TypeClass(String type) {
		switch(type) {
		case "business" :
			return flightfindercont.businessClassRadioBtn;
			
		case "first" : return flightfindercont.firstClassRadioBtn;
		
		case "economy": return flightfindercont.economyClassRadioBtn;
		
		default: 
			return flightfindercont.economyClassRadioBtn;		
		}
		
	}
	
	public WebElement AirlinePref() {
		return flightfindercont.airlinePrefselect;
	}
	
	public WebElement ContinueBtn() {
		return flightfindercont.continueBtn;
	}
	
	
}

package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import pageLocators.ConfirmationPageContent;

public class ConfirmationPage {

	private WebDriver driver;
	public ConfirmationPageContent confpage;
	
	public ConfirmationPage(WebDriver webDriver) {
		driver = webDriver;
		confpage = PageFactory.initElements(driver, ConfirmationPageContent.class);
	}
	
	public boolean confirmationTxt(String string) {
		
		return confpage.confirmationMessageTxt.getText().contains(string);
	}
	
	public boolean DepartingToTxt(String departing, String arriving) {
		if ( confpage.departingToTxt.getText().equalsIgnoreCase(departing+" to "+ arriving))
		{
			return true;
		}
		return false;
	}
	
	public boolean DepartFlight(String departing) {
			if (confpage.departingTxt.getText().contains(departing))
			{
				return true;
			}
			return false;
	}
	
	public boolean DepartFlightClass(String departing) {
		System.out.println(confpage.departingTxt.getText());
			if (confpage.departingTxt.getText().toLowerCase().contains(departing.toLowerCase()))
			{
				return true;
			}
			return false;
	}
	
	public boolean ReturnToTxt(String departing, String arriving) {
		if ( confpage.returningTo.getText().equalsIgnoreCase(departing+" to "+ arriving))
		{
			return true;
		}
		return false;
	}
	
	public boolean ReturnFlight(String departing) {
			if (confpage.returningTxt.getText().contains(departing))
			{
				return true;
			}
			return false;
	}
	
	public boolean ReturntFlightClass(String departing) {
		System.out.println(confpage.returningTxt.getText());
			if (confpage.returningTxt.getText().toLowerCase().contains(departing.toLowerCase()))
			{
				return true;
			}
			return false;
	}

	public boolean Passengers(String num) {
		System.out.println(confpage.passengers.getText());
		if(confpage.passengers.getText().contains(num)) {
			return true;
		}
		return false;
	}

	public boolean billingName(String first, String mid, String last) {
		if(confpage.billText.getText().contains(first+" "+mid+" "+last)) {
			return true;
		}
		return false;
	}
	
	public boolean billAdress1(String address1) {
		if(confpage.billText.getText().contains(address1)) {
			return true;
		}
		return false;
	}
	
	public boolean billAddress2(String address2) {
		if(confpage.billText.getText().contains(address2)) {
			return true;
		}
		return false;
	}
	
	public boolean billcitstatezip(String city, String state, String zip) {
		System.out.println(confpage.billText.getText());
		if(confpage.billText.getText().contains(city+", "+state+", "+zip)) {
			return true;
		}
		return false;
	}
	
	public boolean delAddress1(String address1) {
		if(confpage.DeliveryText.getText().contains(address1)) {
			return true;
		}
		return false;
	}
	
	public boolean delAddress2(String address2) {
		if(confpage.DeliveryText.getText().contains(address2)) {
			return true;
		}
		return false;
	}
	
	public boolean delcitstatezip(String city, String state, String zip) {
		System.out.println(confpage.DeliveryText.getText());
		if(confpage.DeliveryText.getText().contains(city+", "+state+", "+zip)) {
			return true;
		}
		return false;
	}
	
	public void taxes() {
		System.out.println(confpage.totalTaxes.getText());
	}
	
	public void totalPrice() {
		System.out.println(confpage.totalPrice.getText());
	}
	
	public WebElement logout() {
		return confpage.logout;
	}
	
}

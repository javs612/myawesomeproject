package utils;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.WorksheetDocument;

import java.io.File;
import java.io.FileInputStream;

public class ExcelUtils {
	//Crear variables Static para el excel
    //private static XSSFWorkbook excelWBook;
	private static Workbook excelWBook;
	private static org.apache.poi.ss.usermodel.Sheet excelWSheet;
    //private static XSSFSheet excelWSheet;
    private static Cell cell;

	public static Object[][] getTableArray(String FilePath, String SheetName) throws Exception {
		String[][] tabArray = null;
        //FileInputStream excelFile = new FileInputStream(FilePath);
        excelWBook = WorkbookFactory.create(new File(FilePath));
        excelWSheet = excelWBook.getSheet(SheetName);

        int startRow;
        int startCol;
        int totalRows = excelWSheet.getLastRowNum();
        int totalCol = excelWSheet.getRow(0).getLastCellNum();

        tabArray = new String[totalRows][totalCol];
        for(startRow = 1; startRow <= totalRows; startRow++){
            for(startCol = 0; startCol < totalCol; startCol++){
                tabArray[startRow-1][startCol] = getCellDataDDT(startRow, startCol);
            }
        }

        return(tabArray);
    }
	
	public static String getCellDataDDT(int RowNum, int ColNum) throws Exception {
        cell = excelWSheet.getRow(RowNum).getCell(ColNum);
        if(cell.getCellTypeEnum() == CellType.STRING){
            return cell.getStringCellValue();
        }
        if(cell.getCellTypeEnum() == CellType.NUMERIC){
            return String.valueOf(cell.getNumericCellValue());
        }
        return "";
    }
}
